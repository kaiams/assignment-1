const loanBtn = document.getElementById("loanBtn");
const balanceElement = document.getElementById("balance");
const loanedElement = document.getElementById("loaned");
const payElement = document.getElementById("pay");
const workBtn = document.getElementById("workBtn");
const bankBtn = document.getElementById("bankBtn");
const payLoanBtn = document.getElementById("payLoanBtn");

const selectComputer = document.getElementById("selectComp");
const specsElement = document.getElementById("specs");
const compInfo = document.getElementById("compInfo");
const compImage = document.getElementById("compImage");
const compTitle = document.getElementById("compTitle");
const compPrice = document.getElementById("compPrice");
const buyCompBtn = document.getElementById("buyCompBtn");

let balance = 500;
let loaned = 0;
let pay = 0;
let computers = [];
let images = [];

balanceElement.innerText= balance;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => computers = data)
.then(computers => addComputersToList(computers))

let selectedComputer = computers[0];

//helper function gets called when the cmputers are done fetching. 
const addComputersToList = (computers) =>{
    computers.forEach(computer => {
        const computerElement  = document.createElement("option");
        computerElement.value = computer.id;
        computerElement.appendChild(document.createTextNode(computer.title));
        selectComputer.appendChild(computerElement);
        let path = "https://noroff-komputer-store-api.herokuapp.com/"
        path += computer.id == 5? "assets/images/5.png" : computer.image;
        images.push(path);
        changeCompInfo(0);
    })
}

//Administers loan when button clicked.
const handleLoan = () => {
    const loanAmount = prompt("Enter the loan amount", 0);
    if(parseInt(loanAmount) + loaned >= balance * 2){
        console.log(loanAmount, loaned);
        alert("You can't loan more than twice you're balance");
    }
    else if( loanAmount > 0){
        loaned= parseInt(loanAmount);
        balance += loaned;
        balanceElement.innerText= balance;
        loanedElement.innerText= "Loaned: " + loaned + " Kr";
        payLoanBtn.style.display = 'block'
        loanBtn.disabled = true;  
        console.log("You're approved for a loan");     
    }
}

//Adds money to work acount.
const handleWork = () => {
    pay += 100;
    payElement.innerText = pay;
}

//Moves money to Bank acount.
const handleBank = () => {
    if(loaned > 0){
        let subtract = 0.1 * pay;
        if(loaned < subtract){
            subtract = loaned;
            pay -= subtract;
            loanFullyPaid();
        }
        else{
            pay -= subtract;
            loaned -= subtract;
            loanedElement.innerText = "Loaned: " + loaned + " Kr";
        }
        
    }
    balance += pay;
    pay = 0;
    balanceElement.innerText= balance;
    payElement.innerText = pay;
}

//Pays down loan from work acount 
const handlePayLoan = () => {
    if(loaned > pay){
        loaned = loaned - pay;
        pay = 0;
        loanedElement.innerText = "Loaned: " + loaned + " Kr";
    }
    else{
        pay = pay - loaned;
        loanFullyPaid();
    }
    payElement.innerText = pay;
}

//Helper function for when loan is fully paid 
const loanFullyPaid = () => {
    loaned = 0;
    loanedElement.innerText ="";
    payLoanBtn.style.display = "none";
    loanBtn.disabled = false;
}

//gets called from select element on change. 
const handleComputerSelected = e => {
    changeCompInfo(e.target.selectedIndex);
}

//Lists the info of the computer selected in the dropdown
const changeCompInfo = (index) => {
    selectedComputer = computers[index];
    compImage.src = images[index];
    compTitle.innerText = selectedComputer.title;
    compInfo.innerText = selectedComputer.description;
    compPrice.innerText = selectedComputer.price +" Kr";

    let specs = "";
    for(spec in selectedComputer.specs){
         specs += selectedComputer.specs[spec] + "<br />";
        }
    specsElement.innerHTML = specs;
}

//When Buy computer button is pressed
const handleBuyComp = () => {
    if(balance >= parseInt(selectedComputer.price)){
        balance -= parseInt(selectedComputer.price);
        balanceElement.innerText = balance;  
        alert("Congratulations you're now a proud owner of a " + selectedComputer.title);
    }
    else{
        alert("You have insufficent funds! Your missing : " + (parseInt(selectedComputer.price) - balance) + " Kr");
    }
}

//EventListeners on all the needed html objects
workBtn.addEventListener("click", handleWork);
bankBtn.addEventListener("click", handleBank);
loanBtn.addEventListener("click", handleLoan);
payLoanBtn.addEventListener("click", handlePayLoan);
selectComputer.addEventListener("change", handleComputerSelected);
buyCompBtn.addEventListener("click", handleBuyComp);